package rmi.server;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

import rmi.interf.RMIInterface;
import rmi.interf.entities.Car;

public class ServerController extends UnicastRemoteObject implements RMIInterface{
	private static final long serialVersionUID = 1L;

	protected ServerController() throws RemoteException {
		super();
	}

	public double computeTax(Car c) {
		// Dummy formula
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(c.getEngineCapacity() > 1601) sum = 18;
		if(c.getEngineCapacity() > 2001) sum = 72;
		if(c.getEngineCapacity() > 2601) sum = 144;
		if(c.getEngineCapacity() > 3001) sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}

	public double computePriceSelling(Car c) {
//		priceSelling = pricePurchasing - pricePurchasing * (2015 - year) / 7
		Double priceSelling = -1.00;
		if (c != null) {
			priceSelling = c.getPurchasingPrice() - c.getPurchasingPrice() * (2015 - c.getYear()) / 7;
		}
		return priceSelling;
	}
	
	public static void main(String[] args){
		try {
			LocateRegistry.createRegistry(2020);
			Naming.rebind("//localhost:2020/MyServer", new ServerController());
            System.err.println("Server ready");
            
        } catch (Exception e) {
        	System.err.println("Server exception: " + e.toString());
          e.printStackTrace();
        }
	}
}
