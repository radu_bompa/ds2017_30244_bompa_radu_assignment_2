package rmi.interf;

import rmi.interf.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {
	public double computeTax(Car car) throws RemoteException;
	public double computePriceSelling(Car car) throws RemoteException;
}