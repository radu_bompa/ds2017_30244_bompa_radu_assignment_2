package rmi.client.views;

import rmi.interf.entities.Car;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	TaxComputerView is a JFrame which contains the UI elements of the Client application.
 */
public class TaxComputerView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textYear;
	private JTextField textEngineCapacity;
	private JTextField textPurchasingPrice;
	private JButton btnPost;
	private JTextArea textArea;

	public TaxComputerView() {
		setTitle("Car Tax and Price Computer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInsertNewStudent = new JLabel("Car info");
		lblInsertNewStudent.setBounds(10, 11, 120, 14);
		contentPane.add(lblInsertNewStudent);

		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(10, 36, 120, 14);
		contentPane.add(lblYear);

		JLabel lblEngineCapacity = new JLabel("Engine capacity");
		lblEngineCapacity.setBounds(10, 61, 120, 14);
		contentPane.add(lblEngineCapacity);

		JLabel lblPurchasingPrice = new JLabel("Purchasing price");
		lblPurchasingPrice.setBounds(10, 86, 120, 14);
		contentPane.add(lblPurchasingPrice);

		textYear = new JTextField();
		textYear.setBounds(140, 33, 86, 20);
		contentPane.add(textYear);
		textYear.setColumns(10);

		textEngineCapacity = new JTextField();
		textEngineCapacity.setBounds(140, 58, 86, 20);
		contentPane.add(textEngineCapacity);
		textEngineCapacity.setColumns(10);

		textPurchasingPrice = new JTextField();
		textPurchasingPrice.setBounds(140, 83, 86, 20);
		contentPane.add(textPurchasingPrice);
		textPurchasingPrice.setColumns(10);

		btnPost = new JButton("Compute");
		btnPost.setBounds(10, 132, 89, 23);
		contentPane.add(btnPost);

		textArea = new JTextArea();
		textArea.setBounds(235, 131, 171, 120);
		contentPane.add(textArea);
	}

	public void addBtnPostActionListener(ActionListener e) {
		btnPost.addActionListener(e);
	}

	public Integer getYear() {
		try {
			return Integer.parseInt(textYear.getText());
		} catch (Exception e) {
			return 0;
		}
	}

	public Integer getEngineCapacity() {
		try {
			return Integer.parseInt(textEngineCapacity.getText());
		} catch (Exception e) {
			return 0;
		}
	}

	public Double getPurchasingPrice() {
		try {
			return Double.parseDouble(textPurchasingPrice.getText());
		} catch (Exception e) {
			return 0.00;
		}
	}

	public void printTaxAndSellingPrice(Double tax, Double sellingPrice) {
		textArea.setText("Tax: " + tax + ", seling price: " + sellingPrice);
	}

	public void clear() {
		textYear.setText("");
		textEngineCapacity.setText("");
		textPurchasingPrice.setText("");
	}

	public void clearTextArea() {
		textArea.setText("");
	}
}
