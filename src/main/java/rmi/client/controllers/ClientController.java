package rmi.client.controllers;

import rmi.client.views.TaxComputerView;
import rmi.interf.RMIInterface;
import rmi.interf.entities.Car;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class ClientController {
	private static RMIInterface look_up;
	private static TaxComputerView taxComputerView;

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {

		look_up = (RMIInterface) Naming.lookup("//localhost:2020/MyServer");

		taxComputerView = new TaxComputerView();
		taxComputerView.setVisible(true);

		taxComputerView.addBtnPostActionListener(new ComputeActionListener());
	}

	/**
	 * Provides functionality for the Compute button.
	 */
	static class ComputeActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Integer year = taxComputerView.getYear();
			Integer engineCapacity = taxComputerView.getEngineCapacity();
			Double purchasingPrice = taxComputerView.getPurchasingPrice();

			if (year != null && year != 0 && engineCapacity != null && engineCapacity > 0 && purchasingPrice != null && purchasingPrice != 0.00) {
				try {
					Double tax = look_up.computeTax(new Car(taxComputerView.getYear(), taxComputerView.getEngineCapacity(), taxComputerView.getPurchasingPrice()));
					Double priceSelling = look_up.computePriceSelling(
							new Car(taxComputerView.getYear(), taxComputerView.getEngineCapacity(), taxComputerView.getPurchasingPrice()));

					taxComputerView.printTaxAndSellingPrice(tax, priceSelling);
				} catch (RemoteException re) {
					displayErrorMessage("Could not compute tax and selling price! Error: " + re);
				}
			}
		}

		private static void displayErrorMessage(String message) {
			taxComputerView.clear();
			JOptionPane.showMessageDialog(taxComputerView, message, "Error", JOptionPane.ERROR_MESSAGE);
		}

	}
}
